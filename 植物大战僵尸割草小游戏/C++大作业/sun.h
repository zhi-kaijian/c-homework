#ifndef SUN_H
#define SUN_H

#include<graphics.h>

#include"atlas.h"
#include"animation.h"
#include"tool.h"

extern Atlas* atlas_sun;
class Sun
{
public:
	Sun()
	{
		//loadimage(&img, _T("img/sun_1.png"));
		position.x = rand() % 900;
		position.y = rand() % 900;
		pick_regition.left = position.x - 30;
		pick_regition.right = position.x + 30;
		pick_regition.top = position.y - 30;
		pick_regition.bottom = position.y + 30;
		atlasofsun = new Animation(atlas_sun, 45);

	}

	~Sun()
	{
		delete atlasofsun;
	}

	bool PickUp(const Player& player)
	{
		POINT player_position = player.GetPosition();
		if (player_position.x >= pick_regition.left && player_position.x <= pick_regition.right && player_position.y >= pick_regition.top && player_position.y <= pick_regition.bottom)
		{
			return true;
		}
		else
		{
			return false;
		}

	}
	void DrawSun(int delta)
	{
		atlasofsun->Play(position.x, position.y, delta);
	}

private:
	IMAGE img;
	POINT position = { 0,0 };
	RECT pick_regition;
	Animation *atlasofsun;
};



#endif

