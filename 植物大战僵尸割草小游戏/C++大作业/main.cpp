#include<iostream>
#include<graphics.h>

#include"tool.h"
#include"atlas.h"
#include"animation.h"
#include"bullet.h"
#include"button.h"
#include"enemy.h"
#include"player.h"
#include"sun.h"

bool is_game_started = false;
bool running = true;
bool checksun = false;

const int WINDOW_WIDTH = 1280;
const int WINDOW_HEIGHT = 720;

const int BUTTON_WIDTH = 192;
const int BUTTON_HEIGHT = 75;

const int SHADOW_WIDTH = 75;

double bullet_x = 0;
double bullet_y = 0;

int sun_turn = 0;
int sun_number = 0;


#pragma comment(lib,"Winmm.lib")
#pragma comment(lib,"MSIMG32.LIB")

Atlas* atlas_player_left;
Atlas* atlas_player_right;
Atlas* atlas_enemy_left;
Atlas* atlas_enemy_right;
Atlas* atlas_sun;

int idx_current_anim = 0;

const int PLAYER_ANIM_NUM = 6;
const int SUN_ANIM_NUMBER = 5;

IMAGE img_player_left[PLAYER_ANIM_NUM];
IMAGE img_player_right[PLAYER_ANIM_NUM];
IMAGE ing_sun[SUN_ANIM_NUMBER];
 
void TryGenerateEnemy(std::vector<Enemy*>& enemy_list, const Player& player)
{
	if (player.playerlife <= 5)
	{
		const int INTERVAL = 100;
		static int counter = 0;
		if ((++counter) % INTERVAL == 0)
		{
			enemy_list.push_back(new Enemy());
		}
	}
	else
	{
		const int INTERVAL = 300;
		static int counter = 0;
		if ((++counter) % INTERVAL == 0)
		{
			enemy_list.push_back(new Enemy());
		}
	}
}
void UpdateBullets(std::vector<Bullet>& bullet_list, const Player& player, ExMessage& msg)
{
	const double RADIAL_SPEED = 0.0045;
	const double TANGET_SPEED = 0.0045;
	double radian_interval = 2 * 3.14159 / bullet_list.size();
	POINT player_position = player.GetPosition();
	double radius = 100 + 25 * sin(GetTickCount() * RADIAL_SPEED);
	for (size_t i = 0; i < bullet_list.size(); i++)
	{
		double radian = GetTickCount() * RADIAL_SPEED + radian_interval * i;
		bullet_list[i].position.x = player_position.x + 40 + (int)(radius * sin(radian));
		bullet_list[i].position.y = player_position.y + 40 + (int)(radius * cos(radian));
	}

}
void DrawPlayerScore(int score)
{
	static TCHAR text[64];
	_stprintf_s(text, _T("得分：%d"), score);

	setbkmode(TRANSPARENT);
	settextcolor(RGB(255, 85, 185));
	outtextxy(10, 10, text);
}

void DrawPlayerLife(int life)
{
	static TCHAR text[64];
	_stprintf_s(text, _T("生命值：%d"), life);

	setbkmode(TRANSPARENT);
	settextcolor(RGB(255, 85, 185));
	outtextxy(10, 25, text);

}


int main()
{
	initgraph(1280, 720);
	atlas_player_left = new Atlas(_T("img/player_left_%d.png"), 6);
	atlas_player_right = new Atlas(_T("img/player_right_%d.png"), 6);
	atlas_enemy_left = new Atlas(_T("img/enemy_left_%d.png"), 6);
	atlas_enemy_right = new Atlas(_T("img/enemy_right_%d.png"), 6);
	atlas_sun = new Atlas(_T("img/sun_%d.png"), 5);


	mciSendString(_T("open mus/bgm.mp3 alias bgm"), NULL, 0, NULL);
	mciSendString(_T("open mus / hit.wav alias hit"), NULL, 0, NULL);

	int score = 0;
	int bullet_number = 3;

	Player player;
	ExMessage msg;
	IMAGE img_menu;
	IMAGE img_background;

	std::vector<Enemy*> enemy_list;

	RECT region_btn_start_game;
	RECT region_btn_quit_game;

	region_btn_start_game.left = (WINDOW_WIDTH - BUTTON_WIDTH) / 2;
	region_btn_start_game.right = region_btn_start_game.left + BUTTON_WIDTH;
	region_btn_start_game.top = 430;
	region_btn_start_game.bottom = region_btn_start_game.top + BUTTON_HEIGHT;

	region_btn_quit_game.left = (WINDOW_WIDTH - BUTTON_WIDTH) / 2;
	region_btn_quit_game.right = region_btn_quit_game.left + BUTTON_WIDTH;
	region_btn_quit_game.top = 550;
	region_btn_quit_game.bottom = region_btn_quit_game.top + BUTTON_HEIGHT;

	StartGameButton btn_start_game = StartGameButton(region_btn_start_game, _T("img/ui_start_idle.png"), _T("img/ui_start_hovered.png"), _T("img/ui_start_pushed.png"));
	QuitGameButton btn_quit_game = QuitGameButton(region_btn_quit_game, _T("img/ui_quit_idle.png"), _T("img/ui_quit_hovered.png"), _T("img/ui_start_pushed.png"));

	loadimage(&img_menu, _T("img/menu.png"));
	loadimage(&img_background, _T("img/background.png"));

	Sun* sun = new Sun;

	BeginBatchDraw();

	while (running)
	{
		DWORD start_time = GetTickCount();

		std::vector<Bullet>* bullet_list = new std::vector<Bullet>(bullet_number);

		if (checksun == false)
		{
			checksun = true;
			sun_turn++;
			sun_number = 1;
		}
		while (peekmessage(&msg))
		{
			if (is_game_started)
				player.ProcessEvent(msg);
			else
			{
				btn_start_game.ProcessEvent(msg);
				btn_quit_game.ProcessEvent(msg);
			}
		}
		if (is_game_started)
		{
			player.Move();
			UpdateBullets(*bullet_list, player, msg);
			TryGenerateEnemy(enemy_list, player);
			for (Enemy* enemy : enemy_list)
			{
				enemy->Move(player);
			}
			for (Enemy* enemy : enemy_list)
			{
				if (enemy->checkPlayerCollision(player))
				{
					player.playerlife--;
					bullet_number--;
					for (Enemy* enemy : enemy_list)
					{
						enemy->Protect_player(player);
					}
					if (player.playerlife == 0)
					{
						MessageBox(GetHWnd(), _T("gg"), _T("游戏结束"), MB_OK);
						running = false;
					}
					break;
				}
			}
			for (Enemy* enemy : enemy_list)
			{
				for (const Bullet& bullet : *bullet_list)
				{
					if (enemy->CheckBulletCollision(bullet))
					{
						mciSendString(_T("play hit from 0"), NULL, 0, NULL);
						enemy->Hurt(player);
						enemy->Checkdeath();
						if (enemy->Checkdeath())
						{
							score++;
						}
					}

				}
			}
			for (size_t i = 0; i < enemy_list.size(); i++)
			{
				Enemy* enemy = enemy_list[i];
				if (!enemy->Checkalive())
				{
					std::swap(enemy_list[i], enemy_list.back());
					enemy_list.pop_back();
					delete enemy;
				}
			}
		}
		cleardevice();

		if (is_game_started)
		{
			putimage_alpha(0, 0, &img_background);
			player.Draw(1000 / 144);
			for (Enemy* enemy : enemy_list)
			{
				enemy->Draw(1000 / 144);
			}
			for (const Bullet& bullet : *bullet_list)
			{
				bullet.Draw();
			}
			DrawPlayerScore(score);
			DrawPlayerLife(player.playerlife);
			if (checksun == true && score >= 30 * (sun_turn) && score < 30 * (sun_turn + 1) && sun_number == 1)
			{
				sun->DrawSun(1000/144);
			}
			if (sun->PickUp(player))
			{
				player.sunofplayer++;
				if (bullet_number < 5)
				{
					bullet_number++;
				}
				if (player.playerlife < 5)
				{
					player.playerlife++;
				}


			}
			if (sun->PickUp(player) || (score >= 30 * (sun_turn + 1) && score <= 30 * (sun_turn + 2)))
			{
				delete sun;
				sun = new Sun;
				checksun = false;

			}


		}
		else
		{
			putimage(0, 0, &img_menu);
			btn_start_game.Draw();
			btn_quit_game.Draw();
		}

		FlushBatchDraw();

		DWORD end_time = GetTickCount();
		DWORD delta_time = end_time - start_time;
		if (delta_time < 1000 / 144)
		{
			Sleep(1000 / 144 - delta_time);
		}
	}

	delete atlas_player_left;
	delete atlas_player_right;
	delete atlas_enemy_left;
	delete atlas_enemy_right;

	EndBatchDraw();

	return 0;

}
