#ifndef _PLAYER_H
#define _PLAYER_H

#include<graphics.h>

#include"atlas.h"
#include"animation.h"

extern const int WINDOW_WIDTH;
extern const int WINDOW_HEIGHT;


extern Atlas* atlas_player_left;
extern Atlas* atlas_player_right;

class Player
{

public:
	int playerlife = 3;
	int sunofplayer = 3;
	Player()
	{
		anim_left_player = new Animation(atlas_player_left, 45);
		anim_right_player = new Animation(atlas_player_right, 45);
	}
	~Player()
	{
		delete anim_left_player;
		delete anim_right_player;
	}
	void ProcessEvent(const ExMessage& msg)
	{
		switch (msg.message)
		{
		case WM_KEYDOWN:
			switch (msg.vkcode)
			{
			case VK_UP:
				is_move_up = true;
				break;
			case VK_DOWN:
				is_move_down = true;
				break;
			case VK_LEFT:
				is_move_left = true;
				break;
			case VK_RIGHT:
				is_move_right = true;
				break;
			}
			break;
		case WM_KEYUP:
			switch (msg.vkcode) {
			case VK_UP:
				is_move_up = false;
				break;
			case VK_DOWN:
				is_move_down = false;
				break;
			case VK_LEFT:
				is_move_left = false;
				break;
			case VK_RIGHT:
				is_move_right = false;
				break;
			}
			break;

		}

	}
	void Move()
	{
		int dir_x = is_move_right - is_move_left;
		int dir_y = is_move_down - is_move_up;
		double len_dir = sqrt(dir_x * dir_x + dir_y * dir_y);
		if (len_dir != 0 && sunofplayer <= 5)
		{
			double nomalized_x = dir_x / len_dir;
			double nomalized_y = dir_y / len_dir;
			player_pros.x += (int)(PLAYER_SPEED1 * nomalized_x);
			player_pros.y += (int)(PLAYER_SPEED1 * nomalized_y);
		}
		if (len_dir != 0 && sunofplayer > 5)
		{
			double nomalized_x = dir_x / len_dir;
			double nomalized_y = dir_y / len_dir;
			player_pros.x += (int)(PLAYER_SPEED2 * nomalized_x);
			player_pros.y += (int)(PLAYER_SPEED2 * nomalized_y);
		}

		if (player_pros.x < 0) { player_pros.x = 0; };
		if (player_pros.y < 0) { player_pros.y = 0; };
		if (player_pros.x + PLAYER_WIDTH > WINDOW_WIDTH) { player_pros.x = WINDOW_WIDTH - PLAYER_WIDTH; };
		if (player_pros.y + PLAYER_HEIGHT > WINDOW_HEIGHT) { player_pros.y = WINDOW_HEIGHT - PLAYER_HEIGHT; };
	}
	void Draw(int delta)
	{
		static bool facing_left = false;
		int dir_x = is_move_right - is_move_left;
		if (dir_x < 0)
		{
			facing_left = true;
		}
		else
		{
			if (dir_x > 0)
			{
				facing_left = false;
			}
		}
		if (facing_left)
		{
			anim_left_player->Play(player_pros.x, player_pros.y, delta);
		}
		else
		{
			anim_right_player->Play(player_pros.x, player_pros.y, delta);

		}
	}
	const POINT& GetPosition() const
	{
		return player_pros;
	}
private:
	const int PLAYER_SPEED1 = 5;
	const int PLAYER_SPEED2 = 10;
	const int PLAYER_WIDTH = 80;
	const int PLAYER_HEIGHT = 80;
	const int SHADOW_WIDTH = 32;
	Animation* anim_left_player;
	Animation* anim_right_player;
	POINT player_pros = { 500,500 };
	bool is_move_up = false;
	bool is_move_down = false;
	bool is_move_left = false;
	bool is_move_right = false;
};


#endif
