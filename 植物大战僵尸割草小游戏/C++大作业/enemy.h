#ifndef _ENEMY_H
#define _ENEMY_H

#include<graphics.h>

#include"atlas.h"
#include"animation.h"
#include"player.h"
#include"bullet.h"

extern const int WINDOW_WIDTH;
extern const int WINDOW_HEIGHT;
extern const int SHADOW_WIDTH;
extern Atlas* atlas_enemy_left;
extern Atlas* atlas_enemy_right;

class Enemy
{
public:
	Enemy()
	{
		loadimage(&img_shadow, _T("img/shadow_enemy.png"));
		anim_left = new Animation(atlas_enemy_left, 45);
		anim_right = new Animation(atlas_enemy_right, 45);

		enemy_life = 3;

		enum class SpawnEdge
		{
			Up = 0,
			Down,
			Left,
			Right

		};

		SpawnEdge edge = (SpawnEdge)(rand() % 4);
		switch (edge)
		{
		case SpawnEdge::Up:
			position.x = rand() % WINDOW_WIDTH;
			position.y = -FRAME_HEIGHT;
			break;
		case SpawnEdge::Down:
			position.x = rand() % WINDOW_WIDTH;
			position.y = FRAME_HEIGHT;
			break;
		case SpawnEdge::Left:
			position.x = -FRAME_WIDTH;
			position.y = rand() % WINDOW_HEIGHT;
			break;
		case SpawnEdge::Right:
			position.x = FRAME_WIDTH;
			position.y = rand() % WINDOW_HEIGHT;
			break;
		default:
			break;
		}
	}
	bool CheckBulletCollision(const Bullet& bullet)
	{
		bool is_overlap_x = bullet.position.x >= position.x && bullet.position.x <= position.x + FRAME_WIDTH;
		bool is_overlap_y = bullet.position.y >= position.y && bullet.position.y <= position.y + FRAME_HEIGHT;
		return is_overlap_x && is_overlap_y;
	}
	bool checkPlayerCollision(Player& player)
	{
		POINT check_position = { position.x + FRAME_WIDTH / 2,position.y + FRAME_HEIGHT / 2 };
		bool check_x = (check_position.x >= player.GetPosition().x && check_position.x <= player.GetPosition().x + 80);
		bool check_y = (check_position.y >= player.GetPosition().y && check_position.y <= player.GetPosition().y + 80);
		return check_x && check_y;
	}
	void Protect_player(Player& player)
	{
		const POINT& player_position = player.GetPosition();
		RECT protect_region;
		protect_region.right = player_position.x + 100;
		protect_region.left = player_position.x - 100;
		protect_region.top = player_position.y - 100;
		protect_region.bottom = player_position.y + 100;
		if (position.x >= protect_region.left && position.x <= protect_region.right && position.y <= protect_region.bottom && position.y >= protect_region.top)
		{
			int dir_x = player_position.x - position.x;
			int dir_y = player_position.y - position.y;
			double len_dir = sqrt(dir_x * dir_x + dir_y * dir_y);
			double nomalized_x = dir_x / len_dir;
			double nomalized_y = dir_y / len_dir;
			position.x -= (int)(DELTA_DRAWBACK * SPEED * nomalized_x);
			position.y -= (int)(DELTA_DRAWBACK * SPEED * nomalized_y);

		}
	}
	void Move(const Player& player)
	{
		const POINT& player_position = player.GetPosition();
		int dir_x = player_position.x - position.x;
		int dir_y = player_position.y - position.y;
		if (dir_x > 0)
		{
			facing_left = false;
		}
		else
		{
			facing_left = true;
		}
		double len_dir = sqrt(dir_x * dir_x + dir_y * dir_y);
		if (len_dir != 0)
		{
			double nomalized_x = dir_x / len_dir;
			double nomalized_y = dir_y / len_dir;
			position.x += (int)(SPEED * nomalized_x);
			position.y += (int)(SPEED * nomalized_y);
		}

	}

	void Draw(int delta)
	{
		int pos_shadow_x = position.x + (FRAME_WIDTH / 2 - SHADOW_WIDTH / 2);
		int pos_shadow_y = position.y + FRAME_HEIGHT - 35;
		putimage_alpha(pos_shadow_x, pos_shadow_y, &img_shadow);

		if (facing_left)
		{
			anim_left->Play(position.x, position.y, delta);
		}
		else
		{
			anim_right->Play(position.x, position.y, delta);
		}
	}
	~Enemy()
	{
		delete anim_left;
		delete anim_right;
	}
	bool Hurt(const Player& player)
	{
		enemy_life--;
		const POINT& player_position = player.GetPosition();
		int dir_x = player_position.x - position.x;
		int dir_y = player_position.y - position.y;
		double len_dir = sqrt(dir_x * dir_x + dir_y * dir_y);
		double nomalized_x = dir_x / len_dir;
		double nomalized_y = dir_y / len_dir;
		position.x -= (int)(DELTA_DRAWBACK * SPEED * nomalized_x);
		position.y -= (int)(DELTA_DRAWBACK * SPEED * nomalized_y);
		return true;
	}
	bool Checkdeath()
	{
		if (enemy_life == 0)
		{
			alive = false;
			return true;
		}
		else
		{
			return false;
		}
	}
	bool Checkalive()
	{
		return alive;
	}
private:
	const int SPEED = 2;
	const int FRAME_WIDTH = 80;
	const int FRAME_HEIGHT = 80;
	const int DELTA_DRAWBACK = 30;
	int enemy_life;
private:
	IMAGE img_shadow;
	Animation* anim_left;
	Animation* anim_right;
	POINT position = { 0,0 };
	bool facing_left = false;
	bool alive = true;
};



#endif