#include<iostream>
#include<graphics.h>

#include"tool.h"
#include"atlas.h"
#include"animation.h"
#include"bullet.h"
#include"button.h"
#include"enemy.h"
#include"player.h"

bool is_game_started = false;
bool running = true;

const int WINDOW_WIDTH = 1280;
const int WINDOW_HEIGHT = 720;

const int BUTTON_WIDTH = 192;
const int BUTTON_HEIGHT = 75;

const int SHADOW_WIDTH = 75;

double bullet_x = 0;
double bullet_y = 0;


#pragma comment(lib,"Winmm.lib")
#pragma comment(lib,"MSIMG32.LIB")

Atlas* atlas_player_left;
Atlas* atlas_player_right;
Atlas* atlas_enemy_left;
Atlas* atlas_enemy_right;

int idx_current_anim = 0;

const int PLAYER_ANIM_NUM = 6;

IMAGE img_player_left[PLAYER_ANIM_NUM];
IMAGE img_player_right[PLAYER_ANIM_NUM];

void TryGenerateEnemy(std::vector<Enemy*>& enemy_list)
{
	const int INTERVAL = 100;
	static int counter = 0;
	if ((++counter) % INTERVAL == 0)
	{
		enemy_list.push_back(new Enemy());
	}
}
void UpdateBullets(std::vector<Bullet>& bullet_list, const Player& player, ExMessage& msg)
{
	const double RADIAL_SPEED = 0.0045;
	const double TANGET_SPEED = 0.0045;
	double radian_interval = 2 * 3.14159 / bullet_list.size();
	POINT player_position = player.GetPosition();
	double radius = 100 + 25 * sin(GetTickCount() * RADIAL_SPEED);
	for (size_t i = 0; i < bullet_list.size(); i++)
	{
		double radian = GetTickCount() * RADIAL_SPEED + radian_interval * i;
		bullet_list[i].position.x = player_position.x + 40 + (int)(radius * sin(radian));
		bullet_list[i].position.y = player_position.y + 40 + (int)(radius * cos(radian));
	}

}
void DrawPlayerScore(int score)
{
	static TCHAR text[64];
	_stprintf_s(text, _T("�÷֣�%d"), score);

	setbkmode(TRANSPARENT);
	settextcolor(RGB(255, 85, 185));
	outtextxy(10, 10, text);
}


int main()
{
	initgraph(1280, 720);


	BeginBatchDraw();

	while (running)
	{
		DWORD start_time = GetTickCount();


		if (is_game_started)
		{

		}
		cleardevice();

		FlushBatchDraw();

		DWORD end_time = GetTickCount();
		DWORD delta_time = end_time - start_time;
		if (delta_time < 1000 / 144)
		{
			Sleep(1000 / 144 - delta_time);
		}
	}

	delete atlas_player_left;
	delete atlas_player_right;
	delete atlas_enemy_left;
	delete atlas_enemy_right;

	EndBatchDraw();

	return 0;

}
