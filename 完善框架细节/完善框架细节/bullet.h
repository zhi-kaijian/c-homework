#ifndef _BULLET_H
#define _BULLET_H

#include<graphics.h>

class Bullet
{
public:
	POINT position = { 0,0 };
public:
	Bullet() = default;
	~Bullet() = default;

	void Draw()const
	{
		setlinecolor(RGB(255, 155, 50));
		setfillcolor(RGB(127, 255, 0));
		fillcircle(position.x, position.y, RADIUS);
	}
private:
	const int RADIUS = 10;
};

#endif

