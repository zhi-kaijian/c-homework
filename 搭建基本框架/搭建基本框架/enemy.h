#ifndef _ENEMY_H
#define _ENEMY_H

#include<graphics.h>

#include"atlas.h"
#include"animation.h"
#include"player.h"
#include"bullet.h"


class Enemy
{
public:
	Enemy()
	{
		loadimage(&img_shadow, _T("img/shadow_enemy.png"));
		anim_left = new Animation(atlas_enemy_left, 45);
		anim_right = new Animation(atlas_enemy_right, 45);

		enemy_life = 40;

		enum class SpawnEdge
		{
			Up = 0,
			Down,
			Left,
			Right

		};

		SpawnEdge edge = (SpawnEdge)(rand() % 4);
		switch (edge)
		{
		case SpawnEdge::Up:
			position.x = rand() % WINDOW_WIDTH;
			position.y = -FRAME_HEIGHT;
			break;
		case SpawnEdge::Down:
			position.x = rand() % WINDOW_WIDTH;
			position.y = FRAME_HEIGHT;
			break;
		case SpawnEdge::Left:
			position.x = -FRAME_WIDTH;
			position.y = rand() % WINDOW_HEIGHT;
			break;
		case SpawnEdge::Right:
			position.x = FRAME_WIDTH;
			position.y = rand() % WINDOW_HEIGHT;
			break;
		default:
			break;
		}
	}
	bool CheckBulletCollision(const Bullet& bullet)
	{

	}
	bool checkPlayerCollision(const Player& player)
	{

	}
	void Move(const Player& player)
	{

	}

	void Draw(int delta)
	{

	}
	~Enemy()
	{

	}
	void Hurt()
	{

	}
	void Checkdeath()
	{

	}
	bool Checkalive()
	{

	}
private:
	const int SPEED = 2;
	const int FRAME_WIDTH = 80;
	const int FRAME_HEIGHT = 80;
	int enemy_life;
private:
	IMAGE img_shadow;
	Animation* anim_left;
	Animation* anim_right;
	POINT position = { 0,0 };
	bool facing_left = false;
	bool alive = true;
};



#endif
