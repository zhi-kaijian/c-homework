#ifndef _PLAYER_H
#define _PLAYER_H

#include<graphics.h>

#include"atlas.h"
#include"animation.h"

extern const int WINDOW_WIDTH;
extern const int WINDOW_HEIGHT;


extern Atlas* atlas_player_left;
extern Atlas* atlas_player_right;

class Player
{

public:
	Player()
	{

	}
	~Player()
	{

	}
	void ProcessEvent(const ExMessage& msg)
	{


	}
	void Move()
	{

	}
	void Draw(int delta)
	{

	}
	const POINT& GetPosition() const
	{

	}
private:
	const int PLAYER_SPEED = 10;
	const int PLAYER_WIDTH = 80;
	const int PLAYER_HEIGHT = 80;
	const int SHADOW_WIDTH = 32;
	Animation* anim_left_player;
	Animation* anim_right_player;
	POINT player_pros = { 500,500 };
	bool is_move_up = false;
	bool is_move_down = false;
	bool is_move_left = false;
	bool is_move_right = false;
};


#endif

